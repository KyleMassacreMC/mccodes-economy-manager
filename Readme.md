# Economy Manager

This will help you manage your games economy and watch it either grow or fall based on player interaction with the economy. The manager will calculate the game's inflation and adjust prices of things such as items, weapons, armor, etc.

For example, if your economy is too strong (more money ingame than playes have) then everything will be cheaper and *vise verse*. The whole purpose of this module is to accomadate the players and the game owner
___
## Version
1.0.0
___
### Installation

upload the  EconManager.php file to your directory of your choice. I would recommend the classes directory since this is a class file.

This module will self instal on initialization which means, once the module is ran for the first time all the tables in the database will populate and calculate all the money in the game and same goes for crystals. Once you instantiate the crystals class, those tables will populate
___
#### Instantiation

```php
//Instantiate the class
//This must be place inside each file that you are using it in. It is not a one time thing.
include_once('./path_to_your_file/EconManager.php');
//Currency
$mm = new MoneyManager(1000);
//Crystals
$cm = new CrystalManager(10);
```
The MoneyManger and CrystalManager class work very similar to eachother. There are not many differences between the two classes.
___
### Important Methods and Usage
#####Showing inflated price
This is the main method used. It is the meat of the class.
```php
//Both methods return either a float or string depending on the paramer you set which is a bool
$mm->inflatedPrice(true); // Returns 19,000
//And
$cm->inflatedPrice(false); // Returns 19000
```
The parameters for these methods is mainly just for display purposes. By default it is set to false for easy database interaction. When set to true it is to make it pretty for the players. __BE SURE NOT TO SET IT TO TRUE WHEN INTERACTING WITH THE DATABASE__.
#####Showing base prices
Both have a base property.
```php
//Returns the value passed in the constructor
$mm->basePrice; //Returns 1000
$cm->baseAmount; //Returns 10
```
#####Converting Currencies
Both classes have the ability to convert to/from currency/crystals
```php
//Both may return something like .207 || 207.12
$mm->m2c;
$cm->c2m;
```
Both properties will take the price (with inflation) and convert it to currency from crystals or cyrstals to currency.
#####Updating
Inside is a method that can be used to update the database with money currently in the game. __*I*__ chose not to make it dynamic in real time because __*I*__ felt that it would be more like real life if it updated periodically throughout time.

What can be done though, is that you can add this method inside the cron of your choice and it will automatically recalculate the ingame currency amount

```php
include_once('./path_to_your_file/EconManager.php');
/*
* Notice I didn't pass a parameter inside for the constructor. By default
* it is set to 0. This is just an example to update the DB via a cron.
*/
//Money
$mm = new MoneyManager();
//Crystals
$cm = new CrystalManager();
$mm->updateGameMoney();
$cm->updateGameMoney();
```
___

###Some Dos and Donts
#####Dos
* Use this on game items
* Ingame Stores
* Lotteries
* Crimes
* Jobs
* etc... 

You get the idea
#####Donts
* Use this on player to player interactions
* Money transfers
* Crystal transfers
* I wouldnt recommend donation items that contain money/crystals packages

___
[License](http://www.binpress.com/license/view/l/27480f9ff884154c6fb708889e2f418f)
