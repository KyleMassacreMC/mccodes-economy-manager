<?php

/**
 * Class MoneyManager
 * @license http://www.binpress.com/license/view/l/27480f9ff884154c6fb708889e2f418f
 */
class MoneyManager
{

    /**
     * Variable to load the starting price without inflation. This is the value retrieved from the constructor
     * @access public
     * @var int
     */
    public $basePrice;
    /**
     * Variable that will show what the currency is worth dollar for dollar. It is formatted for currency
     * @access public
     * @var string
     */
    public $currencyWorth;
    /**
     * Variable that will convert the cost of the game currency to a crystal amount
     * @access public
     * @var int
     */
    public $m2c;

    /**
     * Variable that stores the array for retrieving the settings from the database
     * @access private
     * @var array
     */
    private $settings = array();

    /**
     * Variable for the database connection
     * @access private
     * @var database
     */
    private $db;


    /**
     * @param int $price optional
     * @default int 0
     */
    public function __construct($price = 0)
    {

        global $db;
        $this->basePrice = $price;
        $this->db = $db;
        $this->settings = $this->__getSettings();
        if (!isset($this->settings['total_ingame_start'])) {
            $this->__install();
        }
        $this->currencyWorth = $this->__getCurrencyWorth();
        $this->m2c = $this->moneyToCrystalsPrice();
    }

    /**
     * Calculates the inflation for the game
     * @return float
     */
    public function getInflation()
    {
        return (float)$this->settings['total_ingame_start'] / ($this->settings['total_ingame'] <= 0 ? 0.1 : $this->settings['total_ingame']);
    }

    /**
     * Calculates the inflated price based on the price set in the constructor
     * @param bool $format
     * @default bool false if set to true it will format the value for currency
     * @return float|string
     */
    public function inflatedPrice($format = false)
    {
        if (!$format) {
            return ($this->getInflation() * $this->basePrice);
        } else {
            return money_formatter(($this->getInflation() * $this->basePrice));
        }
    }


    /**
     * Sets the total money in the database to the current amount of money in the game
     * @return bool
     */
    public function updateGameMoney()
    {
        $getTotal = "SELECT
                      SUM(IF(money > 0,money,0))+SUM(IF(bankmoney > 0,bankmoney,0))+SUM(IF(cybermoney > 0,cybermoney,0))
                      AS `totalMoney`
                      FROM users;";
        $fetch = $this->db->fetch_row($this->db->query($getTotal));
        $update = "UPDATE `economy_settings`
                    SET `total_ingame` = {$fetch['totalMoney']}
                    WHERE `type` = 'money';";
        if($this->db->query($update)) {
            return true;
        }
    }

    /**
     * Method that will convert the cost of the game currency to a crystal amount
     * @alias $m2c
     * @return float
     */
    public function moneyToCrystalsPrice()
    {
        $q = $this->db->query("SELECT `conf_value` FROM `settings` WHERE `conf_name` = 'ct_moneypercrys'");
        $r = $this->db->fetch_single($q);
        return $this->getInflation() * $r;
    }

    /**
     * Loads settings from the database
     * @return array
     */
    private function __getSettings()
    {
        $settings = "SELECT * FROM `economy_settings` WHERE `type` = 'money'";
        $q = $this->db->query($settings);
        while ($r = $this->db->fetch_row($q)) {
            $this->settings = $r;
        }
        return $this->settings;

    }

    /**
     * This will install the required database fields for the module
     */
    private function __install()
    {

        $table = "CREATE TABLE IF NOT EXISTS `economy_settings` (
                      `type` VARCHAR (20) NOT NULL,
                      `total_ingame_start` decimal(38,0) NOT NULL,
                      `total_ingame` decimal(38,0) NOT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1;";
        $this->db->query($table);
        $getTotal = "SELECT
                      SUM(IF(money > 0,money,0))+SUM(IF(bankmoney > 0,bankmoney,0))+SUM(IF(cybermoney > 0,cybermoney,0))
                      AS `totalMoney`
                      FROM users;";
        $fetch = $this->db->fetch_row($this->db->query($getTotal));
        $update = "INSERT INTO `economy_settings`
                    VALUES(
                      'money',{$fetch['totalMoney']},{$fetch['totalMoney']}
                    );";
        $this->db->query($update);
        header("Location: ".$_SERVER['PHP_SELF']);
        exit;

    }

    /**
     * Method that will show what the currency is worth dollar for dollar. It is formatted for currency
     * @param int $amount optional
     * @default int 1
     * @alias $currencyWorth
     * @return string
     */
    private function __getCurrencyWorth($amount = 1)
    {
        return "$" . number_format($this->getInflation() * $amount, 2, '.', '');
    }

}


/**
 * Class CrystalManager
 */
class CrystalManager
{

    /**
     * Variable to load the starting price without inflation. This is the value retrieved from the constructor
     * @access public
     * @var int
     */
    public $baseAmount;
    /**
     * Variable that will show what the currency is worth dollar for dollar. It is formatted for currency
     * @access public
     * @var string
     */
    public $currencyWorth;
    /**
     * Variable that will convert the cost of a crystal amount to currency
     * @access public
     * @var float
     */
    public $c2m;

    /**
     * Variable for the database connection
     * @access private
     * @var database
     */
    private $db;

    /**
     * Variable that stores the array for retrieving the settings from the database
     * @access private
     * @var array
     */
    private $settings = array();


    /**
     * @param int $amount optional
     * @default int 0
     */
    public function __construct($amount = 0)
    {

        global $db;
        $this->baseAmount = $amount;
        $this->db = $db;
        $this->settings = $this->__getSettings();
        $this->c2m = $this->crystalsToMoneyPrice();
        if (!isset($this->settings['total_ingame_start'])) {
            $this->__install();
        }
        $this->currencyWorth = $this->__getCurrencyWorth();


    }

    /**
     * Calculates the inflation for the game
     * @return float
     */
    public function getInflation()
    {
        return (float)$this->settings['total_ingame_start'] / ($this->settings['total_ingame'] <= 0 ? 0.1 : $this->settings['total_ingame']);
    }

    /**
     * Calculates the inflated price based on the amount set in the constructor
     * @param bool $format
     * @default bool false if set to true it will format the value for currency
     * @return int|string
     */
    public function inflatedPrice($format = false)
    {
        if (!$format) {
            return ($this->getInflation() * $this->baseAmount);
        } else {
            return number_format(($this->getInflation() * $this->baseAmount));
        }
    }

    /**
     * Sets the total crystals in the database to the current amount of crystals in the game
     * @return bool
     */
    public function updateGameCrystals()
    {
        $getTotal = "SELECT
                      SUM(IF(crystals > 0,crystals,0))
                      AS `totalCrystals`
                      FROM users;";
        $fetch = $this->db->fetch_row($this->db->query($getTotal));
        $update = "UPDATE `economy_settings`
                    SET `total_ingame` = {$fetch['totalCrystals']}
                    WHERE `type` = 'crystals';";
        if($this->db->query($update)) {
            return true;
        }
    }

    /**
     * Method that will convert the cost of the crystals to a currency
     * @alias $c2m
     * @return float
     */
    public function crystalsToMoneyPrice()
    {
        $q = $this->db->query("SELECT `conf_value` FROM `settings` WHERE `conf_name` = 'ct_moneypercrys'");
        $r = $this->db->fetch_single($q);
        $mm = new MoneyManager($r);
        return (float)$this->getInflation() * $mm->inflatedPrice();
    }

    /**
     * Loads settings from the database
     * @return array
     */
    private function __getSettings()
    {
        $settings = "SELECT `total_ingame_start`, `total_ingame` FROM `economy_settings` WHERE `type` = 'crystals'";
        $q = $this->db->query($settings);
        while ($r = $this->db->fetch_row($q)) {
            $this->settings = $r;
        }
        return $this->settings;

    }

    /**
     * This will install the required database fields for the module
     */
    private function __install()
    {

        $table = "CREATE TABLE IF NOT EXISTS `economy_settings` (
                      `type` VARCHAR (20) NOT NULL,
                      `total_ingame_start` decimal(38,0) NOT NULL,
                      `total_ingame` decimal(38,0) NOT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1;";
        $this->db->query($table);
        $getTotal = "SELECT
                      SUM(IF(crystals > 0,crystals,0))
                      AS `totalCrystals`
                      FROM users;";
        $fetch = $this->db->fetch_row($this->db->query($getTotal));
        $update = "INSERT INTO `economy_settings`
                    VALUES(
                      'crystals',{$fetch['totalCrystals']},{$fetch['totalCrystals']}
                    );";
        $this->db->query($update);
        header("Location: ".$_SERVER['PHP_SELF']);
        exit;

    }

    /**
     * Method that will show what the crystals are worth crystal for crystal. It is formatted for numbers
     * @param int $amount optional
     * @default int 1
     * @alias $currencyWorth
     * @return string
     */
    private function __getCurrencyWorth($amount = 1)
    {
        return number_format($this->getInflation() * $amount, 2, '.', '');
    }

}